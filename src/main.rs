use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Paragraph{
    name: String,
}

#[derive(Serialize, Deserialize)]
struct Article{
    article: String, 
    author: String,
    paragraph: Vec<Paragraph>
}


fn main(){
    let article: Article = Article{
        article: String::from("Alice's Adventures In Wonderland"),
        author: String::from("Lewis Carroll "),
        paragraph: vec![
            Paragraph {
                name: String:: from("If everybody minded their own business, the world would go around a great deal faster than it does.")
            },
            Paragraph {
                name: String:: from("My dear, here we must run as fast as we can, just to stay in place. And if you wish to go anywhere you must run twice as fast as that.")
            },
            Paragraph {
                name: String::from("If you don't know where you are going any road can take you there")
            }
        ]
    };

    let json = serde_json::to_string(&article).unwrap();
    println!("The JSON is {}", json)
}